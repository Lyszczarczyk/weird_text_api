FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python3-pip python-dev build-essential git
RUN pip3 install virtualenv
RUN git clone https://gitlab.com/Lyszczarczyk/weird_text_api.git
WORKDIR weird_text_api
RUN python3 setup.py sdist --format=gztar
RUN pip3 install dist/weird_text*.tar.gz
ENTRYPOINT ["python3"]
CMD ["weird_text_app/weird_text.py"]




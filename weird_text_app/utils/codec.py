"""
author: Damian Lyszczarczyk
"""
import re
from abc import ABCMeta


class Codec:  # pylint: disable=too-few-public-methods
    """
    Base class for encoders and decoders
    """
    __metaclass__ = ABCMeta

    def __init__(self):
        self._find_words = re.compile(r'(\w+)', re.U)
        self._marker = '\n--weird--\n'

    def _get_original_words_from_list(self, raw_words):
        matched_words = list()
        try:
            for word in raw_words:
                matched_words.append(self._find_words.search(word).group())
        except AttributeError:
            raise ValueError('String does not consist of any words!')
        return matched_words

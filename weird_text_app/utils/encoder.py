"""
author: Damian Lyszczarczyk
"""
import random
from copy import deepcopy

from weird_text_app.utils.codec import Codec


class Encoder(Codec):
    """
    class providing string encoding functionality
    """

    def encode(self, text):
        """
        Encodes given string
        :param text:
        :type text: str
        :return: encoded string
        :rtype: str
        """
        raw_words = self._convert_text_to_list(text)
        if not list(filter(None, raw_words)):
            raise ValueError('The string is empty!')
        original_words = self._get_original_words_from_list(raw_words)
        replaced_words = self._encode_words(original_words)
        encoded_text = self._rebuild_encoded_original_text(regexp=self._find_words,
                                                           raw_words=raw_words,
                                                           encoded_words=replaced_words)
        marked_encoded_text = self._mark_encoded_text(encoded_text=encoded_text,
                                                      original_words=original_words,
                                                      encoded_words=replaced_words)
        return marked_encoded_text

    @staticmethod
    def _convert_text_to_list(text):
        return text.split(' ')

    def _encode_words(self, words):
        words_to_replace = deepcopy(words)
        for position, word in enumerate(words_to_replace):
            new_word = self._shuffle_letters(word)
            while self._is_reshuffling_needed(original_word=word, shuffled_word=new_word):
                new_word = self._shuffle_letters(word)
            words_to_replace[position] = new_word
        return words_to_replace

    @staticmethod
    def _rebuild_encoded_original_text(regexp, raw_words, encoded_words):
        final_words = deepcopy(raw_words)
        for position, (word, encoded_word) in enumerate(zip(final_words, encoded_words)):
            final_word = word.replace(regexp.search(word).group(), encoded_word)
            final_words[position] = final_word
        return ' '.join(final_words)

    def _mark_encoded_text(self, encoded_text, original_words, encoded_words):
        decoding_sequence = [word for word, encoded_word in zip(original_words, encoded_words) if word != encoded_word]
        decoding_sequence.sort()
        marked_text = self._marker + encoded_text + self._marker + ' '.join(decoding_sequence)
        return marked_text

    @staticmethod
    def _shuffle_letters(word):
        character_list = list(word[1:-1])
        random.shuffle(character_list)
        replacement_string = ''.join(character_list)
        final_string = word.replace(word[1:-1], replacement_string)
        return final_string

    @staticmethod
    def _is_reshuffling_needed(original_word, shuffled_word):
        is_needed = False
        if len(original_word[1:-1]) < 2:
            is_needed = False
        elif original_word == shuffled_word and not original_word[1:-1].count(original_word[2]) == len(original_word[1:-1]):
            is_needed = True
        return is_needed

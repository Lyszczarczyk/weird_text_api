"""
author: Damian Lyszczarczyk
"""

from copy import deepcopy
from collections import Counter

from weird_text_app.utils.codec import Codec


class Decoder(Codec):
    """
    class providing string decoding functionality
    """
    def decode(self, text):
        """
        Decodes given text
        :param text:
        :type text: str
        :return: decoded text
        :rtype: str
        """
        raw_words = self._extract_and_convert_text_to_list(text)
        original_words = self._get_original_words_from_list(raw_words)
        decoding_sequence = self._get_decoding_sequence(text)
        decoded_words = self._docode_words(original_words, decoding_sequence)
        decoded_text = self._rebuild_decoded_original_text(self._find_words, raw_words, decoded_words)
        return decoded_text

    def _extract_and_convert_text_to_list(self, text):
        try:
            raw_words = text.split(self._marker)[1].split(' ')
        except IndexError:
            raise ValueError('The string is not encoded!')
        return raw_words

    def _get_decoding_sequence(self, text):
        return text.split(self._marker)[-1].split()

    @staticmethod
    def _rebuild_decoded_original_text(regexp, raw_words, decoded_words):
        final_words = deepcopy(raw_words)
        for position, (word, encoded_word) in enumerate(zip(final_words, decoded_words)):
            final_word = word.replace(regexp.search(word).group(), encoded_word)
            final_words[position] = final_word
        return ' '.join(final_words)

    def _docode_words(self, original_words, decoding_sequence):
        decoded_words = deepcopy(original_words)
        for position, original_word in enumerate(original_words):
            for decoding_word in decoding_sequence:
                if self._does_first_and_last_character_match(original_word,
                                                             decoding_word) and self._is_permutation(original_word,
                                                                                                     decoding_word):
                    decoded_words[position] = decoding_word
        return decoded_words

    @staticmethod
    def _does_first_and_last_character_match(original_word, decoding_word):
        are_characters_the_same = [original_word[0] == decoding_word[0], original_word[-1] == decoding_word[-1]]
        return all(are_characters_the_same)

    @staticmethod
    def _is_permutation(original_word, decoding_word):
        original_characters = list(original_word[1:-1])
        decoding_characters = list(decoding_word[1:-1])
        original_characters.sort()
        decoding_characters.sort()
        if len(original_characters) == len(decoding_characters):
            return Counter(original_characters) == Counter(decoding_characters)
        return False

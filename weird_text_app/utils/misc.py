"""
author: Damian Lyszczarczyk
"""
from flask import jsonify


def build_message(status, msg, text):
    """
    Builds json message
    :param status: Http status code
    :param msg:
    :param text: custom text
    :return: json with given parameters
    :rtype: json
    """
    msg_template = dict(status=None, message=None, string=None)

    msg_template['status'] = status
    msg_template['message'] = msg
    msg_template['string'] = text
    return jsonify(msg_template)

"""
author: Damian Lyszczarczyk
"""
import sys
import os
from flask import Flask, make_response
import yaml


from weird_text_app.utils.encoder import Encoder
from weird_text_app.utils.decoder import Decoder
from weird_text_app.utils.misc import build_message

APP = Flask(__name__)
PORT = int(os.environ.get("PORT", 5000))
sys.path.append(os.path.dirname(__file__))


def configure_routes(app):
    """
    Configure routes for given flask app
    :param app:
    :type app: flask.Flask
    """

    @app.route("/spec")
    def spec():  # pylint: disable=unused-variable
        with open('docs.yml') as docs:
            return yaml.load(docs)

    @app.route('/v1/decode/<text>')
    def decode(text):  # pylint: disable=unused-variable
        """
        Endpoint for decoding string
        :param text:
        :return: json response with decoded string
        :rtype json
        """
        text = text.replace('<br>', '\n')
        try:
            output = Decoder().decode(text).replace('\n', '<br>')
            msg = build_message(status=200, msg='OK', text=output)
            return make_response(msg, 200)
        except ValueError as error:
            msg = build_message(status=400, msg=str(error), text=None)
            return make_response(msg, 400)

    @app.route('/v1/encode/<text>')
    def encode(text):  # pylint: disable=unused-variable
        """
        Endpoint for encoding string
        :param text:
        :return: json response with encoded string
        :rtype json
        """
        text = text.replace('<br>', '\n')
        try:
            output = Encoder().encode(text).replace('\n', '<br>')
            msg = build_message(status=200, msg='OK', text=output)
            return make_response(msg, 200)
        except ValueError as error:
            msg = build_message(status=400, msg=str(error), text=None)
            return make_response(msg, 400)


configure_routes(APP)


if __name__ == '__main__':
    APP.run(debug=True, host='0.0.0.0', port=PORT)

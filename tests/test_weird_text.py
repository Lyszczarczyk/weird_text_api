from unittest import TestCase
from flask import Flask

from weird_text_app.weird_text import configure_routes


class TestRoutes(TestCase):
    def setUp(self):
        app = Flask(__name__)
        configure_routes(app)
        self.client = app.test_client()

    def test_encode__should_be_succesful(self):
        url = '/v1/encode/This is a long looong test sentence,<br>with some big (biiiiig) words!'
        response = self.client.get(url)
        self.assertEqual('200 OK', response.status)

    def test_encode__invalid_input_should_return_400(self):
        url = '/v1/encode/!@#$%^&'
        response = self.client.get(url)
        self.assertEqual('400 BAD REQUEST', response.status)

    def test_decode__should_be_succesful(self):
        url = '/v1/decode/<br>--weird--<br>Tihs is a lnog loonog tset seetcnne,<br>with smoe big (biiiiig) wrods!<br>' \
              '--weird--<br>This long looong sentence some test words'
        response = self.client.get(url)
        self.assertEqual('200 OK', response.status)

    def test_decode__invalid_input_should_return_400(self):
        url = '/v1/decode/!@#$%^&'
        response = self.client.get(url)
        self.assertEqual('400 BAD REQUEST', response.status)

import re
from collections import Counter
from unittest import TestCase

from weird_text_app.utils.encoder import Encoder
from weird_text_app.utils.decoder import Decoder


class TestDecoder(TestCase):
    def setUp(self):
        self.decoder = Decoder()
        self.encoder = Encoder()
        self.marker = '\n--weird--\n'
        self.extract_word = re.compile(r'(\w+)', re.U)

    def test_decode__should_be_succesful(self):
        encoder_input = 'This is a long looong test sentence,\nwith some big (biiiiig) words!'
        encoder_output = self.encoder.encode(encoder_input)

        decoder_output = self.decoder.decode(encoder_output)
        markers_check = decoder_output.count(self.marker)
        self.assertEqual(0, markers_check)
        self.assertTrue(Counter(encoder_input) == Counter(decoder_output))

    def test_decode_empty_string__should_raise_error(self):
        text = ''
        with self.assertRaises(ValueError):
            output = self.decoder.decode(text)

    def test_decode_not_marked_string__should_raise_error(self):
        text = 'Tihs is a lnog loonog tset snntceee,' \
               'wtih smoe big (biiiiig) wdros!'
        with self.assertRaises(ValueError):
            output = self.decoder.decode(text)

    def test_decode_only_special_characters__should_raise_error(self):
        text = '!@#$%^&*('
        with self.assertRaises(ValueError):
            output = self.decoder.decode(text)

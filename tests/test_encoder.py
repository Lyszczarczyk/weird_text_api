import re
from unittest import TestCase

from weird_text_app.utils.encoder import Encoder


class TestEncoder(TestCase):
    def setUp(self):
        self.encoder = Encoder()
        self.marker = '\n--weird--\n'
        self.extract_word = re.compile(r'(\w+)', re.U)

    def test_encode__should_be_succesful(self):
        text = 'This is a long looong test sentence,\nwith some big (biiiiig) words!'
        output = self.encoder.encode(text)
        markers_check = output.count(self.marker)
        self.assertEqual(2, markers_check)
        self.assertTrue(self._are_words_shuffled(text, output))
        self.assertTrue(self._does_sorted_list_exist(text, output))
        self.assertNotEqual(text, output)

    def test_encode_empty_string__should_raise_error(self):
        text = ''
        with self.assertRaises(ValueError):
            output = self.encoder.encode(text)

    def test_encode_only_special_characters__should_raise_error(self):
        text = '!@#$%^&*('
        with self.assertRaises(ValueError):
            output = self.encoder.encode(text)

    def _are_words_shuffled(self, original_text, output_text):
        cut_marker = output_text.split(self.marker)
        shuffled_words = cut_marker[1].split(' ')
        original_words = original_text.split(' ')
        decoding_sequence = cut_marker[-1].split()
        for nr, original_word in enumerate(original_words):
            if self.extract_word.search(original_word).group() not in decoding_sequence:
                original_words[nr] = None
                shuffled_words[nr] = None
        original_words = list(filter(None, original_words))
        shuffled_words = list(filter(None, shuffled_words))
        are_shuffled = [original_word != shuffled_word for original_word, shuffled_word in zip(original_words, shuffled_words)]
        return all(are_shuffled)

    def _does_sorted_list_exist(self, original_text, output_text):
        cut_marker = output_text.split(self.marker)
        shuffled_words = cut_marker[1].split(' ')
        shuffled_words = ' '.join(shuffled_words)
        original_words = original_text.split(' ')
        original_words = ' '.join(original_words)
        decoding_sequence = cut_marker[-1].split()
        shuffled_words = self.extract_word.findall(shuffled_words)
        original_words = self.extract_word.findall(original_words)
        expected_decoding_sequence = [original_word for original_word, shuffled_word in zip(original_words,
                                                                                            shuffled_words) if original_word != shuffled_word]
        expected_decoding_sequence.sort()
        return expected_decoding_sequence == decoding_sequence
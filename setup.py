from distutils.core import setup
# pylint: skip-file

with open('requirements/base.txt') as f:
    required = f.read().splitlines()

setup(
    name='weird_text_api',
    version='0.0.1',
    packages=['weird_text_app', 'weird_text_app/utils'],
    data_files=['docs.yml', 'requirements/base.txt'],
    install_requires=required
)